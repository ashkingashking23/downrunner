using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject Normal, Breakable, Spiked;
    [SerializeField] GameObject[] Speedups;

    static float LeftBound=-2.17f, RightBound=2.17f,SpawnTimer=1.8f,CurrentSpawnTimer;

    static int SpawnCount=0;

    void Start()
    {
        CurrentSpawnTimer = SpawnTimer;   
    }

    
    void Update()
    {
        SpawnPlatforms();
    }

    void SpawnPlatforms()
    {
        CurrentSpawnTimer += Time.deltaTime;
        if (CurrentSpawnTimer >= SpawnTimer)                          //to compare realtime with spawn time 
        {
            SpawnCount++;

            Vector3 tempPoint= transform.position;
            tempPoint.x = Random.Range(LeftBound, RightBound);

            GameObject NewPlatformTemp = null;
            
            switch (SpawnCount)
            {
                case 3:
                    if (Random.Range(0, 2) > 0)
                        NewPlatformTemp = Instantiate(Speedups[Random.Range(0, Speedups.Length)], tempPoint, Quaternion.identity);
                    else
                        NewPlatformTemp = Instantiate(Normal, tempPoint, Quaternion.identity);
                    break;
                case 4:
                    if (Random.Range(0, 2) > 0)
                        NewPlatformTemp = Instantiate(Normal, tempPoint, Quaternion.identity);
                    else
                        NewPlatformTemp = Instantiate(Spiked, tempPoint, Quaternion.identity);
                    break;
                case 5:
                    if (Random.Range(0, 2) > 0)
                        NewPlatformTemp = Instantiate(Breakable, tempPoint, Quaternion.identity);
                    else
                        NewPlatformTemp = Instantiate(Normal, tempPoint, Quaternion.identity);
                    break;
                case 6:
                    if (Random.Range(0, 2) > 0)
                        NewPlatformTemp = Instantiate(Normal, tempPoint, Quaternion.identity);
                    else
                        NewPlatformTemp = Instantiate(Breakable, tempPoint, Quaternion.identity);
                    SpawnCount = 0;
                    break;
                default:
                    NewPlatformTemp = Instantiate(Normal, tempPoint, Quaternion.identity);
                    break;
            }

            CurrentSpawnTimer = 0f;
            NewPlatformTemp.transform.parent = transform;
        }
        ////////-----------REset Timer----------/////////// 
    }
}









#region Help
/*
            if (SpawnCount < 3)
                NewPlatformTemp = Instantiate(Normal, tempPoint, Quaternion.identity);

            else if (SpawnCount == 3)                                //spawn a normal or randomize
            {
                if (Random.Range(0, 2) > 0)
                    NewPlatformTemp = Instantiate(Normal, tempPoint, Quaternion.identity);
                else
                    NewPlatformTemp = Instantiate(Speedups[Random.Range(0, Speedups.Length)], tempPoint, Quaternion.identity);
            }
            else if (SpawnCount == 4)                                //Randomizing platforms
            {
                if (Random.Range(0, 2) > 0)
                    NewPlatformTemp = Instantiate(Normal, tempPoint, Quaternion.identity);
                else
                    NewPlatformTemp = Instantiate(Spiked, tempPoint, Quaternion.identity);
            }
            else if (SpawnCount == 5)                                //Randomizing platforms
            {
                if (Random.Range(0, 2) > 0)
                    NewPlatformTemp = Instantiate(Breakable, tempPoint, Quaternion.identity);
                else
                    NewPlatformTemp = Instantiate(Normal, tempPoint, Quaternion.identity);
            }
            else if (SpawnCount == 6)                                //Randomizing platforms
            {
                if (Random.Range(0, 2) > 0)
                    NewPlatformTemp = Instantiate(Spiked, tempPoint, Quaternion.identity);
                else
                    NewPlatformTemp = Instantiate(Breakable, tempPoint, Quaternion.identity);
                SpawnCount = 0;                     //////////////////// reset the count!!!!
            }*/
#endregion