using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BG_Script : MonoBehaviour
{
    float scrollspeed = 2f;

    void Start()
    {
        
    }

    private void Awake()
    {
    }

    void Update()
    {
        transform.Translate(new Vector2(0, -1f) * scrollspeed * Time.deltaTime);
        if (transform.position.y < -10f)
        {
            transform.position = new Vector2(transform.position.x, 9.4f);
        }
    }
}
