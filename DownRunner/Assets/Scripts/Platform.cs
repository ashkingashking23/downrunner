using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    float scrollspeed = 1f;

    Animator Animator_script;
    [SerializeField] bool NormalPlatorm, SpikedPlatform, BreakablePlatform, LeftSpeedPlatform, RightSpeedPlatform;

    // Start is called before the first frame update
    void Awake()
    {
        Animator_script = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        transform.Translate(Vector2.up * scrollspeed * Time.smoothDeltaTime);
        if (transform.position.y > 5.5f)
            //  gameObject.SetActive(false);
            Destroy(gameObject);
    }
}
