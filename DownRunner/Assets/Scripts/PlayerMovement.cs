using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D PlayerRB;
    Animator PlayerAnim;

    float Movespeed = 3f, Left_bound = -2.62f, Right_bound = 2.62f;

    private void Awake()
    {
        PlayerRB = gameObject.GetComponent<Rigidbody2D>();
        PlayerAnim = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        BoundsManager();
        PlayerMovementAnim();
    }

    void BoundsManager()
        {
            Vector2 BoundaryChecker = transform.position;
            if (BoundaryChecker.x < Left_bound)
                BoundaryChecker.x = Left_bound;
            else if (BoundaryChecker.x > Right_bound)
                BoundaryChecker.x = Right_bound;
            transform.position = BoundaryChecker;
        if(transform.position.y < -5.51f)
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LeftClick()
    {
        PlayerRB.velocity = new Vector2(Movespeed, PlayerRB.velocity.y) * -1f;
        PlayerRB.velocity = PlayerRB.velocity.normalized * Movespeed;
    }
    public void RightClick()
    {
        PlayerRB.velocity = new Vector2(Movespeed, PlayerRB.velocity.y);
        PlayerRB.velocity = PlayerRB.velocity.normalized * Movespeed;
    }

    public void ResetBut()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Spikes"))
        {
            Time.timeScale = 0f;
        }
    }

    #region ANimator

    void PlayerMovementAnim()
    {
        PlayerAnim.SetFloat("Speed", PlayerRB.velocity.magnitude);
    }

    #endregion
}
