using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] GameObject body;
    [SerializeField] Vector3 positions;

    private void Start()
    {
        positions = body.transform.position;
    }

    private void Update()
    {
       

        #region KeyboardInput
        if (Input.GetKeyDown(KeyCode.X)) // or if (Input.GetKeyDown("X"))
            Debug.Log("Pressed X");

        if (Input.GetKeyUp(KeyCode.Q))
            Debug.Log("Released Q");

        if (Input.GetKey(KeyCode.T))
            Debug.Log("T held");

        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Running!!");
        }
        #endregion

        #region MouseButton
        //if (Input.GetMouseButtonUp(0))    //1 for right click 2 for middle mb
        //{
        //    Debug.Log("Leftclick");
        //}
        #endregion

        #region Buttons
        //if(Input.GetButtonDown("Fire1"))
        //{
        //    Debug.Log("Firing!!!");
        //}
        #endregion

        #region AxisInput

        #endregion
        

        #region TouchInput
        if (Input.touchCount > 0)
        {
            //Touch t1 = Input.GetTouch(0);
            //    Debug.Log("Getting touch at "+t1.position + " & "+t1.phase);
            if (Input.touchCount == 1)
            {
                Touch t1 = Input.GetTouch(0);
                if (t1.phase == TouchPhase.Began)
                {
                    body.transform.position = t1.position;
                }
                else if (t1.phase == TouchPhase.Moved)
                {
                    body.transform.position = t1.position;
                }
                else if (t1.phase == TouchPhase.Ended)
                {
                    body.transform.position = positions;
                }
            }
            
        }
        #endregion
    }
}
